package com.hugy.test.service;

import com.hugy.test.model.Account;
import com.hugy.test.model.Permissions;
import com.hugy.test.model.Role;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.hugy.test.config.Constants.localCache;

@Service
public class UserService {
    public List<Permissions> findPermissionListByUserId(String userid) {
        Set<Role> roles = localCache.get(userid).getRoles();
        List<Permissions> list = new ArrayList<>(3);
        for (Role role : roles) {
            list.addAll(role.getPermissions());
        }
        return list;
    }

    public Account getUserByName(String name) {
        return localCache.get(name);
    }

}
