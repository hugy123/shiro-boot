package com.hugy.test.service;

import com.hugy.test.model.Role;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.Set;

import static com.hugy.test.config.Constants.localCache;

@Service
public class RoleService {
    public String findRoleByUserId(String userid) {
        Set<Role> roles = localCache.get(userid).getRoles();
        if(CollectionUtils.isNotEmpty(roles)){
            return roles.toArray()[0].toString();
        }
        return null;
    }
}
