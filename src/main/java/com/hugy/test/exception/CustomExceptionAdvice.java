package com.hugy.test.exception;

import com.hugy.test.config.ResponseResult;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理类
 */
@RestControllerAdvice
public class CustomExceptionAdvice {

    @ExceptionHandler(UnknownAccountException.class)
    public ResponseResult xxx(UnknownAccountException e){
        return new ResponseResult(-1000,"账号不存在，请重新输入",null);
    }

   @ExceptionHandler(IncorrectCredentialsException.class)
    public  ResponseResult password(IncorrectCredentialsException e){
        return new ResponseResult(-1001,"密码错误，请重新输入",null);
   }

    @ExceptionHandler(ClassCastException.class)
    public  ResponseResult password(ClassCastException e){
        return new ResponseResult(-1024,e.getMessage(),null);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseResult aa(UnauthorizedException e){
        return new ResponseResult(-1000,"权限不足，亲联系管理员",null);
    }

    @ExceptionHandler(LockedAccountException.class)
    public ResponseResult zzz(LockedAccountException e){
        System.out.println("===============================================");
        return new ResponseResult(-1002,e.getMessage(),null);
    }

    @ExceptionHandler(ExcessiveAttemptsException.class)
    public ResponseResult eee(ExcessiveAttemptsException e){
        return new ResponseResult(-1003,e.getMessage(),null);
    }


}

