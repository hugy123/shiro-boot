package com.hugy.test.config;

import com.hugy.test.model.Account;
import com.hugy.test.model.Permissions;
import com.hugy.test.model.Role;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Constants {
    public static ConcurrentHashMap<String,Account> localCache = new ConcurrentHashMap<>(4);

    static {
        // 创建用户1
        Set<Permissions> permissionsSet = new HashSet<>(2);
        permissionsSet.add(new Permissions("1", "query"));
        permissionsSet.add(new Permissions("2", "add"));

        Role role = new Role("1", "admin", permissionsSet);
        Set<Role> roleSet = new HashSet<>(1);
        roleSet.add(role);
        Account user = new Account("11", "wang", "123456", roleSet);
        localCache.put("11", user);
        localCache.put("wang", user);

        Set<Permissions> permissionsSet1 = new HashSet<>(1);
        permissionsSet1.add(new Permissions("3", "del"));
        Role role1 = new Role("2", "user", permissionsSet1);
        Set<Role> roleSet1 = new HashSet<>(1);
        roleSet1.add(role1);
        Account user1 = new Account("12", "zhang", "123456", roleSet1);
        localCache.put("12", user);
        localCache.put("zhang", user);
    }
}
