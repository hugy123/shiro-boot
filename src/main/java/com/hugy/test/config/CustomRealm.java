package com.hugy.test.config;

import com.hugy.test.model.Account;
import com.hugy.test.model.ActiveUser;
import com.hugy.test.model.Permissions;
import com.hugy.test.service.RoleService;
import com.hugy.test.service.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * created on 2016年9月6日 
 *
 * 自定义shiro realm
 *
 * @author  hymes
 * @version  0.0.1
 */

public class CustomRealm extends AuthorizingRealm {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Resource
	private UserService userService;
	
	@Resource
	private RoleService roleService;
	
	@Override
	public void setName(String name) {
		super.setName("customRealm");
	}

	/**
	 * realm的  用户认证方法，从数据库查询用户信息
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		
		// token是用户输入的用户名和密码,第一步从token中取出用户名
		String username = (String) token.getPrincipal();
		
		// 第二步：根据用户输入的username从数据库查询
		Account sysUser = null;
		
		try {
			sysUser = userService.getUserByName(username);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		// 如果查询不到返回null
		if(sysUser==null){
			if (logger.isDebugEnabled()){
				logger.debug("user not exist!");
			}
			return null;
		}
		
		String password = sysUser.getPassword();

		// 如果查询到返回认证信息AuthenticationInfo
		//activeUser就是用户身份信息
		ActiveUser activeUser = new ActiveUser();
		
		activeUser.setUserid(String.valueOf(sysUser.getId()));
		activeUser.setUsername(sysUser.getUserName());


		String roleName = null;
		List<Permissions> permissionList = null;
		try {
			permissionList = userService.findPermissionListByUserId(activeUser.getUserid());
			roleName = roleService.findRoleByUserId(sysUser.getId());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		activeUser.setRolename(roleName);
		activeUser.setRoleStatus("1");
		activeUser.setPermissions(permissionList);

		logger.info(activeUser.getUsername());
		
		//ByteSource q = ByteSource.Util.bytes(sysUser.getSalt());
		//将activeUser设置simpleAuthenticationInfo
		SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(
				activeUser, password, this.getName());
		simpleAuthenticationInfo.setCredentialsSalt(ByteSource.Util.bytes("hymes"));
		return simpleAuthenticationInfo;
	}

	/**
	 * realm的 授权 方法
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		//从 principals获取主身份信息
		//将getPrimaryPrincipal方法返回值转为真实身份类型（在上边的doGetAuthenticationInfo认证通过填充到SimpleAuthenticationInfo中身份类型），
		ActiveUser activeUser =  (ActiveUser) principals.getPrimaryPrincipal();
		
		//根据身份信息从数据库获取到权限数据
		List<Permissions> permissionList = null;
		try {
			permissionList = userService.findPermissionListByUserId(activeUser.getUserid());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		List<String> permissions = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(permissionList)){
			permissionList.forEach(sysPermission -> permissions.add(sysPermission.getPermission()));
		}
		
		//查到权限数据，返回授权信息(要包括 上边的permissions)
		SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
		//将上边查询到授权信息填充到simpleAuthorizationInfo对象中
		simpleAuthorizationInfo.addStringPermissions(permissions);

		return simpleAuthorizationInfo;
	}
	
	//清除缓存
	public void clearCached() {
		PrincipalCollection principals = SecurityUtils.getSubject().getPrincipals();
		super.clearCache(principals);
	}
}