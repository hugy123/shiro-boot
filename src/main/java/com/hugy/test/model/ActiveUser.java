package com.hugy.test.model;

import lombok.Data;

import java.util.List;

@Data
public class ActiveUser {
   private String userid;
   private String username;
   private boolean userStatus;
   private String rolename;
   private String roleStatus;
   private List<Permissions> permissions;
   private List<Permissions> menus;
}
